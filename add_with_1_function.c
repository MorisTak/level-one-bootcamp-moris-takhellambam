//Write a program to add two user input numbers using one function.

#include<stdio.h>

float add(float a, float b){
	float sum = a + b;
	return sum;
}

int main(){
	float x, y;

	printf("Enter the two numbers.");
	scanf("%f %f", &x, &y);
	
	add(x,y);
	
	printf("The sum is %f", add);
	
	return 0;
}
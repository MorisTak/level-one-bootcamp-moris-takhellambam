//Write a program to find the sum of n different numbers using 4 functions


#include<stdio.h>

typedef struct arrContainer 
{
    int n;
    float nos[30];
}arrCon;

arrCon fetch()
{
    arrCon loc;
    int i;
    
    printf("Enter the no. of numbers to be added: ");
    scanf("%d", &loc.n);
    
    printf("Enter the number: ");
    
    for( i=0 ; i<loc.n ; i++)
    {
        scanf("%f", &loc.nos[i]);
    }
    
    return loc;
}

float add(arrCon loc)
{
    int i;
    float sum = 0;
    
    for( i=0 ; i<loc.n ; i++)
    {
        sum = sum + loc.nos[i];
    }
    return sum;
}

void print(float sum)
{
    printf("The sum of the numbers entered is %.3f", sum);
}

int main()
{
    arrCon inMain = fetch();
    
    float sum = add(inMain);
    
    print(sum);
    
    return 0;
}

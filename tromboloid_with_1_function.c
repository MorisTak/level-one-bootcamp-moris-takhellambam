//Write a program to find the volume of a tromboloid using one function

#include<stdio.h>

int main(){
    float h,d,b;
    
    printf("Enter the value of h, d and b of the tromboloid: ");
    scanf("%f %f %f", &h, &d, &b);
    
    float volume = ((h*d)+d)/(3*b);
    
    printf("The volume of the tromboloid is %f", volume);
    
    return 0;
}
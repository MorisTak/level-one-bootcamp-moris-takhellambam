//WAP to find the volume of a tromboloid using 4 functions.

#include<stdio.h>

float fetch(char arr[30])
{
    float input;
    printf("Enter the value of %s: ", arr);
    scanf("%f", &input);
    return input;
}

float volume(float h,float d, float b)
{
    float vol = ((h*d)+d)/(3*b);
    return vol;
}

void printVol(float a)
{
    printf("The volume of the tromboloid is %f", a);
}

int main()
{
    float height, dim, breadth, vol;
    
    height = fetch("height");
    
    dim = fetch("characteristic dimension");
    
    breadth = fetch("breadth");
    
    vol = volume(height,dim,breadth);
    
    printVol(vol);
    
    return 0;
}
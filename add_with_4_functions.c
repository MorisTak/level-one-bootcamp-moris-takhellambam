//Write a program to add two user input numbers using 4 functions.

#include<stdio.h>

int input(){
    int a;
    printf("Enter a no.: ");
    scanf("%d", &a);
    return a;
}

int add(int a,int b){
    int result = a + b;
    return result;
}

void output(int a, int b, int c){
    printf("The sum of %d and %d is %d", a, b, c);
}

int main(){
    int n1, n2, sum;
    
    n1 = input();
    n2 = input();
    
    sum = add(n1, n2);
    
    output(n1,n2,sum);
    
    return 0;
}
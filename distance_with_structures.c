//WAP to find the distance between two points using structures and 4 functions.

#include<stdio.h>
#include<math.h>

typedef struct point
{
    float x, y;
}point;

point inputCoor(char arr[])
{
    point pt;
    
    printf("Enter the coordinates of the %s pt.: ", arr);
    
    scanf("%f %f", &pt.x, &pt.y);
    return pt;
}

float distance(point a, point b)
{
    float dist = sqrt((a.x-b.x)*(a.x-b.x) + (a.y-b.y)*(a.y-b.y));
    return dist;
}

void outputDist(point a, point b, float dist)
{
    printf("The distance between (%.2f,%.2f) and (%.2f,%.2f) is %.2f", a.x, a.y, b.x, b.y,  dist);
}

int main()
{
    point p1;
    point p2;
    char a[3] = "1st";
    char b[3] = "2nd";
    
    p1 = inputCoor(a);
     
    p2 = inputCoor(b);
    
    float dist = distance(p1, p2);
    
    outputDist(p1, p2, dist);
    
    return 0;
}
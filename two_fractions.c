//WAP to find the sum of two fractions.

#include<stdio.h>

typedef struct fraction
{
    int numo,deno;
}fraction;

fraction fetch(char arr[3])
{
    fraction frac;
    
    printf("Enter the numerator of the %s fraction: ", arr);
    scanf("%d", &frac.numo);
    
    printf("Enter the denominator of the %s fraction: ", arr);
    scanf("%d", &frac.deno);
    
    return frac;
}

int gcd(int a, int b)
{
    int temp;
    
    while (b != 0) 
    {
        temp = b;
        b = a % b;
        a = temp;
    }
    return temp;
}

fraction add(fraction a, fraction b)
{
    fraction sum;
	sum.numo = (a.numo)*(b.deno)+(b.numo)*(a.deno);
	sum.deno = (a.deno)*(b.deno);

    return sum;
}

fraction simplify(fraction sum)
{
    int factor = gcd(sum.numo, sum.deno);
       
    sum.numo = sum.numo/factor;
    sum.deno = sum.deno/factor;
       
    return sum;
}

void print(fraction a, fraction b, fraction sum)
{
    printf("The sum of %d/%d and %d/%d is %d/%d", a.numo, a.deno, b.numo, b.deno, sum.numo, sum.deno);
}

int main()
{
    fraction first = fetch("1st");
    fraction second = fetch("2nd");
    
    fraction sum = add(first, second);
    
    sum = simplify(sum);
    
    print(first, second, sum);
    
    return 0;
}